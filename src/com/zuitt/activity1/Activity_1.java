package com.zuitt.activity1;
import java.util.Scanner;

public class Activity_1 {
    public static void main(String[] args) {

        String fName = "Giovanni";
        String lName = "Giorgio";
        String fullName = fName + " " + lName;
        int English = 88;
        int Math = 82;
        int Science = 85;

        double average = (English + Math + Science)/3;

        System.out.print("Name: " + fullName);
        System.out.print(System.getProperty("line.separator"));
        System.out.print("Average: " + average);


    }

}
